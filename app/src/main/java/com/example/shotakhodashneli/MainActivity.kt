package com.example.shotakhodashneli


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.text.isDigitsOnly

class MainActivity : AppCompatActivity() {

    lateinit var phonenumber: EditText
    lateinit var piradinomeri: EditText
    lateinit var name: EditText
    lateinit var surname: EditText
    lateinit var check: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        phonenumber = findViewById(R.id.phonenumber)
        piradinomeri = findViewById(R.id.piradinomeri)
        name = findViewById(R.id.nameText)
        surname = findViewById(R.id.surnameText)
        check = findViewById(R.id.check)

    }

    fun register(clickedView: View) {

        if (phonenumber.text.isNotEmpty() && name.text.isNotEmpty() && surname.text.isNotEmpty() && piradinomeri.text.isNotEmpty()) {

            if (phonenumber.text.isDigitsOnly()) {

                if (check.isChecked) {

                    Toast.makeText(applicationContext, "წარმატებულაი", Toast.LENGTH_SHORT).show()

                } else {

                    Toast.makeText(applicationContext, "Check License... :d", Toast.LENGTH_SHORT).show()

                }

            } else {
                Toast.makeText(applicationContext, "არასწორი ნომერია", Toast.LENGTH_SHORT).show()
            }

        } else {

            Toast.makeText(applicationContext, "გთხოვტ შეავსოთ ფორმა.", Toast.LENGTH_SHORT).show()

        }

    }

}